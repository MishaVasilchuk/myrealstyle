from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.views import PasswordChangeView
from django.db.models import Prefetch, Q
from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, HttpRequest
from django.template import loader
from django.contrib import messages
from django.urls import reverse_lazy, reverse
from django.views.generic import ListView, DetailView, CreateView, View, UpdateView
from django.contrib.auth import login, logout

from .forms import UserRegisterForm, NewsForm, UserLoginForm, CommentsForm, EditProfileForm, AddProfileInformation, \
    UserSettingsForm, ChangePasswordForm, EditNewsForm
from mrs_base.models import News, Category, Comment, User, Profile, BlogTips, Themes, Tags


def index(request):
    template = loader.get_template('indexbase.html')
    return HttpResponse(template.render({}, request))


def training(request):
    template = loader.get_template('training.html')
    return HttpResponse(template.render({}, request))


def about(request):
    template = loader.get_template('about.html')
    return HttpResponse(template.render({}, request))


def portfolio_index(request):
    template = loader.get_template('portfolio-index.html')
    return HttpResponse(template.render({}, request))


def portfolio(request):
    template = loader.get_template('portfolio.html')
    return HttpResponse(template.render({}, request))


def casual(request):
    template = loader.get_template('casual.html')
    return HttpResponse(template.render({}, request))


def brutal(request):
    template = loader.get_template('brutal.html')
    return HttpResponse(template.render({}, request))


def dandy(request):
    template = loader.get_template('dandy.html')
    return HttpResponse(template.render({}, request))


def mystyle(request):
    template = loader.get_template('mystyle.html')
    return HttpResponse(template.render({}, request))


def mystyle2(request):
    template = loader.get_template('mystyle2.html')
    return HttpResponse(template.render({}, request))


@login_required
def podbor(request):
    template = loader.get_template('podbor.html')
    return HttpResponse(template.render({}, request))


@login_required
def podborcvety(request):
    template = loader.get_template('podborcvetov.html')
    return HttpResponse(template.render({}, request))


class BlogNews(ListView):
    model = News
    template_name = 'blog.html'
    context_object_name = 'news'
    paginate_by = 5

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tips'] = BlogTips.objects.filter(is_published=True)
        return context

    def get_queryset(self):
        return News.objects.filter(theme=1, is_published=True)


class NewsCategory(ListView):
    model = News
    template_name = 'blog_category.html'
    context_object_name = 'news'
    allow_empty = False
    paginate_by = 5

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['categories_topic'] = Category.objects.get(pk=self.kwargs['topic_id'])
        return context

    def get_queryset(self):
        return News.objects.filter(topic_id=self.kwargs['topic_id'], is_published=True).select_related('topic')


class NewsTags(ListView):
    model = News
    template_name = 'blog_tag.html'
    context_object_name = 'news'
    allow_empty = False
    paginate_by = 5

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tags'] = Tags.objects.get(pk=self.kwargs['tag_id'])
        return context

    def get_queryset(self):
        return News.objects.filter(tag_id=self.kwargs['tag_id'], is_published=True)


class AddComment(View):

    def post(self, request, *args, **kwargs):
        print(request.POST)
        form = CommentsForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('mrs_base:ViewPost', news_id=request.POST.get('post'))


class ViewPost(DetailView):
    model = News
    pk_url_kwarg = 'news_id'
    template_name = 'blog_post.html'
    context_object_name = 'news_item'

    def get_queryset(self):
        return News.objects.prefetch_related(
            Prefetch('comments', queryset=Comment.objects.all())
        )


class AddPost(CreateView):
    form_class = NewsForm
    template_name = 'add_post.html'
    login_url = '/'


class EditPost(UpdateView):
    model = News
    pk_url_kwarg = 'news_id'
    template_name = 'edit_post.html'
    context_object_name = 'news_item'
    form_class = EditNewsForm

    def get_object(self):
        return get_object_or_404(News, id=self.kwargs.get('id'))


def user_logout(request):
    if request.method == "POST":
        logout(request)
        return redirect('index')


def user_login(request):
    if request.method == 'POST':
        form = UserLoginForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect('index')
    else:
        form = UserLoginForm()
    template = loader.get_template('login.html')
    return HttpResponse(template.render({'form': form}, request))


class UserRegisterView(CreateView):
    form_class = UserRegisterForm
    template_name = 'signup.html'
    success_url = reverse_lazy('index')


# def register(request):
#     if request.method == 'POST':
#         form = UserRegisterForm(request.POST)
#         if form.is_valid():
#             user = form.save()
#             login(request, user)
#             messages.success(request, 'Вы успешно зарегистрированы!')
#             return redirect('index')
#         else:
#             messages.error(request, 'Ошибка регистрации!')
#     else:
#         form = UserRegisterForm()
#     template = loader.get_template('signup.html')
#     return HttpResponse(template.render({'form': form}, request))


class Search(ListView):
    model = News
    paginate_by = 5
    template_name = 'blog.html'
    context_object_name = 'news'

    def get_queryset(self):
        return News.objects.filter(Q(short_text__icontains=self.request.GET.get('q')) |
                                   Q(title__icontains=self.request.GET.get('q')))

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['q'] = f'&q={self.request.GET.get("q")}'
        return context


class ViewProfile(DetailView):
    model = User
    pk_url_kwarg = 'username'
    template_name = 'profile.html'
    context_object_name = 'user'

    def get_object(self):
        return get_object_or_404(User, username=self.kwargs.get('username'))


class EditProfilePage(UpdateView):
    model = Profile
    pk_url_kwarg = 'user'
    template_name = 'edit_profile_page.html'
    context_object_name = 'profile'
    form_class = EditProfileForm

    def get_object(self):
        return get_object_or_404(Profile, user_id=self.kwargs.get('user_id'))


class AddProfileInformation(CreateView):
    model = Profile
    form_class = AddProfileInformation
    template_name = 'add_profile_information.html'

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


class SettingsProfile(UpdateView):
    form_class = UserSettingsForm
    template_name = 'settings_profile.html'

    def get_object(self):
        return self.request.user

    def get_success_url(self):
        return reverse('mrs_base:Profile', kwargs={'username': self.request.user.username})


class ChangePasswordView(PasswordChangeView):
    form_class = ChangePasswordForm
    template_name = 'change_password.html'

    def get_success_url(self):
        return reverse('mrs_base:Profile', kwargs={'username': self.request.user.username})
