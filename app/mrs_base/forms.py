from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm, UserChangeForm, PasswordChangeForm
from django.contrib.auth.models import User
from django import forms
from django.forms import widgets

from mrs_base.models import Category, News, Comment, Profile
from mrs_base.widgets import MyWidget

user = get_user_model()


class NewsForm(forms.ModelForm):
    is_published = forms.ChoiceField(choices=(('True', 'Да'), ('False', 'Нет')))

    class Meta:
        model = News
        fields = ('title', 'theme', 'tag', 'short_text', 'content', 'is_published', 'topic', 'photo')
        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control'}),
            'short_text': forms.TextInput(attrs={'class': 'form-control'}),
            'content': forms.Textarea(attrs={'rows': 1, 'cols': 1}),
            'topic': forms.Select(),
            # 'photo': MyWidget()

        }


class EditNewsForm(forms.ModelForm):
    is_published = forms.ChoiceField(choices=(('True', 'Да'), ('False', 'Нет')))

    class Meta:
        model = News
        fields = ('title', 'theme', 'tag', 'short_text', 'content', 'is_published', 'topic', 'photo')
        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control'}),
            'short_text': forms.TextInput(attrs={'class': 'form-control'}),
            'content': forms.Textarea(attrs={'rows': 1, 'cols': 1}),
            'topic': forms.Select(),

        }


class AddProfileInformation(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('bio', 'telegram_nikname', 'mobile_phone', 'profile_pic')
        widgets = {
            'mobile_phone': forms.TextInput(attrs={'class': 'form-control'}),
            'telegram_nikname': forms.TextInput(attrs={'class': 'form-control'}),
            'bio': forms.Textarea(attrs={'rows': 1, 'cols': 1}),
        }


class EditProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('bio', 'telegram_nikname', 'mobile_phone', 'profile_pic')
        widgets = {
            'mobile_phone': forms.TextInput(attrs={'class': 'form-control'}),
            'telegram_nikname': forms.TextInput(attrs={'class': 'form-control'}),
            'bio': forms.Textarea(attrs={'rows': 1, 'cols': 1}),
        }


class CommentsForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('content', 'author', 'post')


class UserRegisterForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2')

    username = forms.CharField(label='Логин пользователя', widget=forms.TextInput(attrs={
        'class': 'wrap-input100 validate-input m-t-5 m-b-10',
    }))
    email = forms.EmailField(label='E-mail', widget=forms.EmailInput(attrs={
        'class': 'wrap-input100 validate-input m-t-5 m-b-10'
    }))
    first_name = forms.CharField(label='Имя', widget=forms.TextInput(attrs={
        'class': 'wrap-input100 validate-input m-t-5 m-b-10',
    }))
    last_name = forms.CharField(label='Фамилия', widget=forms.TextInput(attrs={
        'class': 'wrap-input100 validate-input m-t-5 m-b-10',
    }))
    password1 = forms.CharField(label='Пароль', widget=forms.PasswordInput(attrs={
        'class': 'wrap-input100 validate-input m-b-10'
    }))
    password2 = forms.CharField(label='Подтверждение пароля', widget=forms.PasswordInput(attrs={
        'class': 'wrap-input100 validate-input m-b-10'
    }))


class UserLoginForm(AuthenticationForm):
    username = forms.CharField(label='Имя пользователя', widget=forms.TextInput(attrs={
        'class': 'wrap-input100 validate-input m-t-5 m-b-10',
    }))
    password = forms.CharField(label='Пароль', widget=forms.PasswordInput(attrs={
        'class': 'wrap-input100 validate-input m-b-10'
    }))


class UserSettingsForm(UserChangeForm):
    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name')

    username = forms.CharField(max_length=100, label='Имя пользователя', widget=forms.TextInput(attrs={
        'class': 'wrap-input100 validate-input m-t-5 m-b-10',
    }))
    first_name = forms.CharField(max_length=100, label='Имя', widget=forms.TextInput(attrs={
        'class': 'wrap-input100 validate-input m-t-5 m-b-10',
    }))
    last_name = forms.CharField(max_length=100, label='Фамилия', widget=forms.TextInput(attrs={
        'class': 'wrap-input100 validate-input m-t-5 m-b-10',
    }))
    email = forms.EmailField(label='E-mail', widget=forms.EmailInput(attrs={
        'class': 'wrap-input100 validate-input m-t-5 m-b-10'
    }))


class ChangePasswordForm(PasswordChangeForm):
    class Meta:
        model = User
        fields = ('old_password', 'new_password1', 'new_password2')

    old_password = forms.CharField(max_length=100, label='Старый пароль', widget=forms.PasswordInput(attrs={
        'class': 'wrap-input100 validate-input m-t-5 m-b-10', 'type': 'password'
    }))
    new_password1 = forms.CharField(max_length=100, label='Новый пароль', widget=forms.PasswordInput(attrs={
        'class': 'wrap-input100 validate-input m-t-5 m-b-10', 'type': 'password'
    }))
    new_password2 = forms.CharField(max_length=100, label='Повторите новый пароль', widget=forms.PasswordInput(attrs={
        'class': 'wrap-input100 validate-input m-t-5 m-b-10', 'type': 'password'
    }))
