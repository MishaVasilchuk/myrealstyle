"""mrs URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from django.contrib.auth.views import LogoutView, PasswordChangeView
from mrs.settings import LOGOUT_REDIRECT_URL

from mrs_base.views import training, about, portfolio_index, portfolio, \
    casual, brutal, dandy, mystyle, mystyle2, logout, UserRegisterView, user_login,  \
    BlogNews, NewsCategory, ViewPost, AddPost, EditPost, podbor, podborcvety, AddComment, \
    Search, ViewProfile, EditProfilePage, AddProfileInformation, SettingsProfile, NewsTags, \
    ChangePasswordView

app_name = 'mrs_base'

urlpatterns = [
    path('training/', training, name='training'),
    path('about/', about, name='about'),
    path('portfolio_index/', portfolio_index, name='portfolio_index'),
    path('portfolio/', portfolio, name='portfolio'),
    path('casual/', casual, name='casual'),
    path('brutal/', brutal, name='brutal'),
    path('dandy/', dandy, name='dandy'),
    path('mystyle/', mystyle, name='mystyle'),
    path('mystyle2/', mystyle2, name='mystyle2'),
    path('login/', user_login, name='login'),
    path('logout/', LogoutView.as_view(next_page=LOGOUT_REDIRECT_URL), name='logout'),
    path('register/', UserRegisterView.as_view(), name='register'),
    path('blog/', BlogNews.as_view(), name='BlogNews'),
    path('blog/category/<int:topic_id>/', NewsCategory.as_view(), name='NewsCategory'),
    path('blog/tag/<int:tag_id>/', NewsTags.as_view(), name='NewsTags'),
    path('blog/news_post/<int:news_id>', ViewPost.as_view(), name='ViewPost'),
    path('blog/edit_post/<int:id>', EditPost.as_view(), name='EditPost'),
    path('add-post', AddPost.as_view(), name='AddPost'),
    path('podbor/', podbor, name='podbor'),
    path('podborcvety/', podborcvety, name='podborcvety'),
    path('add-comment/', AddComment.as_view(), name='AddComment'),
    path('blog/search/', Search.as_view(), name='Search'),
    path('profile/user/<username>', ViewProfile.as_view(), name='Profile'),
    path('profile/user/edit_profile_page/<int:user_id>', EditProfilePage.as_view(), name='EditProfilePage'),
    path('profile/user/add_profile_information/', AddProfileInformation.as_view(), name='AddProfileInformation'),
    path('profile/user/settings/', SettingsProfile.as_view(), name='SettingsProfile'),
    path('profile/user/settings/changepassword', ChangePasswordView.as_view(), name='ChangePassword'),
]
