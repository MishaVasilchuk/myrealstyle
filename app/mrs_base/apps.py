from django.apps import AppConfig


class MrsBaseConfig(AppConfig):
    name = 'mrs_base'
    verbose_name = 'MRS'
