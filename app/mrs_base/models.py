from django.contrib.auth import get_user_model
from django.db import models
from django.urls import reverse

User = get_user_model()


class Category(models.Model):
    topic = models.CharField(max_length=120, null=False, blank=False, db_index=True,
                             verbose_name='Категории')

    def get_absolute_url(self):
        return reverse('mrs_base:NewsCategory', kwargs={'topic_id': self.pk})

    def __str__(self):
        return self.topic

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'
        ordering = ['topic']


class Themes(models.Model):
    theme_choices = (
        ('Мужской стиль', 'Мужской стиль'),
        ('Спорт', 'Спорт'),
        ('Отношения', 'Отношения'),
        ('Личностный рост', 'Личностный рост')
    )
    theme = models.CharField(max_length=120, choices=theme_choices, null=False, blank=False, db_index=True,
                             verbose_name='Тема')

    def __str__(self):
        return self.theme

    class Meta:
        verbose_name = 'Тема'
        verbose_name_plural = 'Темы'
        ordering = ['theme']


class Tags(models.Model):
    theme = models.ForeignKey(Themes, on_delete=models.CASCADE, null=False, blank=False, verbose_name='Тема')
    name_tag = models.CharField(max_length=120, null=False, blank=False, verbose_name='Название тега')

    def get_absolute_url(self):
        return reverse('mrs_base:NewsTags', kwargs={'tag_id': self.pk})

    def __str__(self):
        return self.name_tag

    class Meta:
        verbose_name = 'Тег'
        verbose_name_plural = 'Теги'
        ordering = ['theme']


class News(models.Model):
    theme = models.ForeignKey(Themes, on_delete=models.CASCADE, null=False, blank=False, verbose_name='Тема')
    tag = models.ForeignKey(Tags, on_delete=models.CASCADE,  null=False, blank=False, verbose_name='Тег')
    title = models.CharField(max_length=120, verbose_name='Заголовок')
    short_text = models.TextField(max_length=150, blank=True, verbose_name='Вступление')
    content = models.TextField(blank=True, verbose_name='Контент')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата публикации')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Обновлено')
    photo = models.ImageField(upload_to='media/blog/photos/blog/%Y/%m/%d/', verbose_name='Изображение', null=False,
                              blank=False)
    is_published = models.BooleanField(default=True, verbose_name='Статус публикации')
    topic = models.ForeignKey(Category, on_delete=models.PROTECT, null=False, verbose_name='Категория')

    def get_absolute_url(self):
        return reverse('mrs_base:ViewPost', kwargs={'news_id': self.pk})

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'
        ordering = ['-created_at']


class Comment(models.Model):
    post = models.ForeignKey(News, on_delete=models.CASCADE, null=False, related_name='comments',
                             verbose_name='Новость')
    author = models.CharField(max_length=120, null=False, blank=False, verbose_name='Имя')
    content = models.TextField(blank=True, verbose_name='Контент')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата публикации')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Обновлено')

    def get_absolute_url(self):
        return reverse('mrs_base:ViewPost', kwargs={'news_id': self.post_id})

    def __str__(self):
        return self.author

    class Meta:
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Комментарии'
        ordering = ['created_at']


class Profile(models.Model):
    user = models.OneToOneField(User, null=True, on_delete=models.CASCADE)
    bio = models.TextField()
    profile_pic = models.ImageField(upload_to='media/profile/photos/', verbose_name='Изображение профиля', null=True,
                                    blank=True)
    telegram_nikname = models.CharField(max_length=120, null=True, blank=True)
    mobile_phone = models.CharField(max_length=10, null=True, blank=True)

    def __str__(self):
        return str(self.user)

    def get_absolute_url(self):
        return reverse('mrs_base:Profile', kwargs={'username': self.user.username})

    class Meta:
        verbose_name = 'Информация пользователя'
        verbose_name_plural = 'Информация пользователя'


class BlogTips(models.Model):
    tip_of_day = models.TextField(max_length=120, verbose_name='Совет дня')
    is_published = models.BooleanField(default=True, verbose_name='Статус публикации')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата публикации')

    def __str__(self):
        return str(self.tip_of_day)

    class Meta:
        verbose_name = 'Советы дня'
        verbose_name_plural = 'Советы дня'
        ordering = ['-created_at']
