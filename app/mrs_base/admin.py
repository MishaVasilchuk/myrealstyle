from django.contrib import admin
from django.utils.safestring import mark_safe

from mrs_base.models import News, Category, Comment, Profile, BlogTips, Themes, Tags


class NewsAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'theme', 'topic', 'tag', 'created_at', 'updated_at', 'is_published',)
    list_display_links = ('id', 'title', 'theme', 'tag')
    search_fields = ('title', 'content', 'theme', 'tag')
    list_editable = ('is_published',)
    list_filter = ('is_published', 'topic', 'theme', 'tag')
    fields = (
    'title', 'theme', 'topic', 'tag', 'short_text', 'content', 'is_published', 'photo', 'created_at', 'updated_at',
    'get_photo',
    'views')
    readonly_fields = ('get_photo', 'views', 'created_at', 'updated_at')

    def get_photo(self, obj):
        return mark_safe(f'<img src="{obj.photo.url}" width=75>')

    get_photo.short_description = 'Изображение'


# class CommentsAdmin(admin.ModelAdmin):
#     list_display = ('id', 'author', 'post', 'created_at', 'updated_at',)
#     list_display_links = ('id', 'author')
#     search_fields = ('content',)
#     list_filter = ('post', 'author',)
#     fields = ('post', 'content', 'author', 'created_at', 'updated_at')
#     readonly_fields = ('author', 'created_at', 'updated_at')


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'topic')
    list_display_links = ('id', 'topic')
    search_fields = ('title', 'topic')


class ThemesAdmin(admin.ModelAdmin):
    list_display = ('id', 'theme')
    list_display_links = ('id', 'theme')
    search_fields = ('title', 'theme')


class TagsAdmin(admin.ModelAdmin):
    list_display = ('id', 'theme', 'name_tag')
    list_display_links = ('id', 'theme', 'name_tag')
    search_fields = ('title', 'theme', 'name_tag')


class TipsAdmin(admin.ModelAdmin):
    list_display = ('id', 'tip_of_day', 'is_published', 'created_at',)
    list_display_links = ('tip_of_day', 'id',)
    search_fields = ('tip_of_day',)
    list_editable = ('is_published',)
    list_filter = ('is_published', 'tip_of_day',)
    fields = ('is_published', 'tip_of_day',)


admin.site.register(News, NewsAdmin)
admin.site.register(Comment)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Profile)
admin.site.register(BlogTips, TipsAdmin)
admin.site.register(Themes, ThemesAdmin)
admin.site.register(Tags, TagsAdmin)

admin.site.site_title = 'MRS - Управление'
admin.site.site_header = 'MRS - Управление'
