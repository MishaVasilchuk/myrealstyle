from django import template
from mrs_base.models import Category, Tags

register = template.Library()


@register.simple_tag()
def get_categories():
    return Category.objects.all()


@register.simple_tag()
def get_tags():
    return Tags.objects.filter(theme=1)

